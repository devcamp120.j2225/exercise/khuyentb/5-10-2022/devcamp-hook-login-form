import { Grid, TextField, Typography, Button } from "@mui/material"
import { useState } from "react";

function Login() {
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("")
    return (
        <Grid container mt={3} sx={{ alignItems: "center", justifyContent: "center" }}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography variant="h3" style={{ textAlign: "center" }} component="div" >
                    Welcome Back!
                </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} mt={2} >
                <TextField id="inp-username" fullWidth label="email" variant="outlined" onChange={(event) => setUserName(event.target.value)} value={userName}></TextField>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} mt={2} >
                <TextField id="inp-password" fullWidth label="password" variant="outlined" onChange={(event) => setPassword(event.target.value)} value={password}></TextField>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                <Button color="success" fullWidth variant="contained" id="btn-login">Login</Button>
            </Grid>
        </Grid>
    )
}

export default Login