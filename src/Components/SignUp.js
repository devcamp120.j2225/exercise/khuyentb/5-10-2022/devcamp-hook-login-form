import { Grid, TextField, Typography, Button } from "@mui/material"
import { useState } from "react";

function SignUp() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("")
    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")
    return (
        <Grid container mt={3} sx={{ alignItems: "center", justifyContent: "center" }}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography variant="h3" style={{ textAlign: "center" }} component="div" >
                    Sign Up for Free!
                </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6} mt={2} paddingRight={1}>
                <TextField id="inp-firstname" fullWidth label="First Name" variant="outlined" onChange={(event) => setFirstName(event.target.value)} value={firstName}></TextField>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6} mt={2} paddingLeft={1}>
                <TextField id="inp-lastname" fullWidth label="Last Name" variant="outlined" onChange={(event) => setLastName(event.target.value)} value={lastName}></TextField>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} mt={2} >
                <TextField id="inp-username" fullWidth label="Email Address" variant="outlined" onChange={(event) => setUserName(event.target.value)} value={userName}></TextField>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} mt={2} >
                <TextField id="inp-password" fullWidth label="Set A Password" variant="outlined" onChange={(event) => setPassword(event.target.value)} value={password}></TextField>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                <Button color="success" fullWidth variant="contained" id="btn-login">Login</Button>
            </Grid>
        </Grid>
    )
}

export default SignUp