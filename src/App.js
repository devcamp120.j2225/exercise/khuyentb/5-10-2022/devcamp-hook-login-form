
import { Container, Button, ButtonGroup, Grid } from '@mui/material';
import { useState } from 'react';
import Login from './Components/Login';
import SignUp from './Components/SignUp';

function App() {
  const [isLogin, setIsLogin] = useState(true);

  return (
    <Container>
      <Grid container sx={{display: "flex", justifyContent: "center", height: "100vh"}}>
        <Grid item padding={5} xs={12} sm={12} md={6} lg={6} mt={3} sx={{backgroundColor: "#bdb9b6"}}>
          <ButtonGroup variant="contained" fullWidth mt={5} aria-label="outlined primary button group">
            <Button color={(isLogin) ? "inherit" : "success"} onClick={() => setIsLogin(false)}>Sign Up</Button>
            <Button color={(isLogin) ? "success" : "inherit"} onClick={() => setIsLogin(true)}>Login</Button>
          </ButtonGroup>
          {
            (isLogin) ? <Login></Login> : <SignUp></SignUp>
          }
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
